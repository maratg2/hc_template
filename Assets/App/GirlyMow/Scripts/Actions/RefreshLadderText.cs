using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class RefreshLadderText : CoreAction
    {
        [InjectComponent] private LadderTextProvider _ladderTextProvider;
        
        protected override bool Action()
        {
            for (int i = 0; i < _ladderTextProvider.Texts.Count; i++)
            {
                _ladderTextProvider.Texts[i].text = (i+1) + "x";
                _ladderTextProvider.Texts[i].enabled = false;
            }
            return true;
        }
    }
}