using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;


namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class ShowField : CoreComponent
    {
        public InputSignal onStart;
        public OutputSignal onEndShowing;
        private Vector3 _startPos;
        [SerializeField] private Vector3 _endPos;
        [SerializeField, NotNull] private Transform _playerTransform;
        [SerializeField, NotNull] private Transform _meshesTransform;
        [SerializeField, NotNull] private GameObject _mowerObject;
        [SerializeField, NotNull] private FollowBeforeBridge _follower;
        private bool showingFlag;
        
        private void OnEnable()
        {
            onStart.InvokeAction += StartShowing;
        }

        private void OnDisable()
        {
            onStart.InvokeAction -= StartShowing;
        }

        private void StartShowing()
        {
            if (!showingFlag)
                StartCoroutine(Showing());
        }

        private IEnumerator Showing()
        {
            showingFlag = true;
            _startPos = _playerTransform.position;
            _mowerObject.SetActive(false);
            
            float angle = Vector2.SignedAngle(new Vector2(_startPos.x, _startPos.z),
                new Vector2(_endPos.x, _endPos.z));
            float t = 0f, time = 1.4f;
            _meshesTransform.rotation = Quaternion.Euler(new Vector3(0f, -angle, 0f));
            _follower.OnBeginFollow(time);
            while (t < 1f)
            {
                _playerTransform.position = Vector3.Lerp(_startPos, _endPos, t);
                t += Time.deltaTime / time;
                yield return new WaitForEndOfFrame();
            }
            onEndShowing?.Invoke();
        }
    }
}