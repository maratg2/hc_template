using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Actions;
using Krem.AppCore.Joystick.Components;
using UnityEngine;

[NodeGraphGroupName("HW2")] 
public class RigidbodyOwnSetVel : RigidbodySetVelocity 
{
    [InjectComponent] private RigidbodyMovable _rigidbodyMovable; 
        
    protected override bool Action()
    {
        Vector3 movementVector =
            new Vector3(_rigidbodyMovable.InputAxis.Axis.x, 0f, _rigidbodyMovable.InputAxis.Axis.y);
        _rigidbodyMovable.Rigidbody.velocity = movementVector * _rigidbodyMovable.sensitivity;
        return true;
    }
}
