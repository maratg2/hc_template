using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ChangeAss : CoreAction
    {
        [InjectComponent] private AssProvider _assProvider;
        private int countTaken = 0;
        public InputData<float> assScale;
        [ActionParameter] public bool isGrass;
        [ActionParameter] public int foodCount = 20;
        [ActionParameter] public Vector3 beginSize;
        [ActionParameter] public Vector3 size1;
        [ActionParameter] public Vector3 size2;
        [ActionParameter] public Vector3 size3;
        private bool _isGameEnded;
        private bool _isSet;
        private BarProgress _bar;
        
        protected override bool Action()
        {
            countTaken++;
            if(!_bar)
                _bar = GameObject.FindObjectOfType<BarProgress>();
            if (isGrass)
            {
                _bar.UpdateBarImage((float)countTaken / foodCount);
                float t1, t2, t3;
                t1 = countTaken / (foodCount / 3f);
                t2 = (countTaken - foodCount / 3f) / (foodCount / 3f);
                t3 = (countTaken - foodCount / 3f * 2f) / (foodCount / 3f);
                Vector3 newScale = beginSize;
                
                if (countTaken <= foodCount / 3)
                    newScale = Vector3.Lerp(beginSize, size1, t1);
                else if (countTaken <= foodCount / 3 * 2)
                    newScale = Vector3.Lerp(size1, size2, t2);
                else if (countTaken <= foodCount)
                    newScale = Vector3.Lerp(size2, size3, t3);

                if (t3 > 1f)
                    newScale = Vector3.Lerp(size2, size3, 1);
                        
                _assProvider.Ass.localScale = newScale;
                
                if (!_isGameEnded && PlayerPrefs.GetInt("Grass") <= 0)
                {
                    _isGameEnded = true;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}