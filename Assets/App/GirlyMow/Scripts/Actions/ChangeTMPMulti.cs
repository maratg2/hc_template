using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ChangeTMPMulti : CoreAction
    {
        [InjectComponent] private TMPLink _tmpLink;
        public InputData<int> multi;
        protected override bool Action()
        {
            _tmpLink.Text.text = multi.Data + "x";
            return true;
        }
    }

}