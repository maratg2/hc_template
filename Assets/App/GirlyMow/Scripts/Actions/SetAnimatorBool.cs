using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class SetAnimatorBool : CoreAction
    {
        [InjectComponent] private AnimatorProvider _animatorProvider;
        [ActionParameter] public string boolStateName;
        public InputData<bool> isMoving;

        protected override bool Action()
        {
            _animatorProvider.animator.SetBool(boolStateName, isMoving.Data);
            return true;
        }
    }
}