using System;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Actions;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class CheckTriggerWithGrass : CoreAction
    {
        public InputData<Collider> grassCollider;
        protected override bool Action()
        {
            if(grassCollider.Data != null)
            {
                UnityEngine.GameObject.Destroy(grassCollider.Data.gameObject);
                return true;
            }

            return false;
        }
    }
}