using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Components;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class CheckAxisNotZero : CoreAction
    {
        [InjectComponent] private ScriptableAxis2DDecorator _axis2DDecorator;
        public InputComponent<StatePlayerMoving> statePlayerMoving;
        protected override bool Action()
        {
            statePlayerMoving.Component._isMoving.Data = _axis2DDecorator.Axis != Vector2.zero;
            return true;
        }
    }
}