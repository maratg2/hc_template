using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class RefreshLvlMoney : CoreAction
    {
        [InjectComponent] private MoneyProvider _moneyProvider;
        public InputComponent<TMPLink> _tmp;
        protected override bool Action()
        {
            _tmp.Component.Text.text = '+' + _moneyProvider.MoneyLvl.ToString();
            return true;
        }
    }
}