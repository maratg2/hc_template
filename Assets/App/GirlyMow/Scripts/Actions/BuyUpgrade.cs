using System;
using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class BuyUpgrade : CoreAction
    {
        [ActionParameter] public UpgradeType upgradeType;
        [InjectComponent] private MoneyProvider _moneyProvider;
        [InjectComponent] private UpgradesProvider _upgradesProvider;
        
        protected override bool Action()
        {
            int value = 0;
            switch (upgradeType)
            {
                case UpgradeType.Butt:
                    if (_upgradesProvider.ButtLevelModel >= _upgradesProvider.ButtPrices.Count)
                    {
                        Debug.LogError("Index out of butt prices bounds");
                        return false;
                    }
                    
                    value = _upgradesProvider.ButtPrices[_upgradesProvider.ButtLevelModel].Value;
                    if (_moneyProvider.Money >= value)
                    {
                        _moneyProvider.Money -= value;
                        _upgradesProvider.ButtLevelModel++;
                        return true;
                    }
                    break;
                case UpgradeType.Distance:
                    if (_upgradesProvider.ButtLevelModel >= _upgradesProvider.ButtPrices.Count)
                    {
                        Debug.LogError("Index out of distance prices bounds");
                        return false;
                    }
                    
                    value = _upgradesProvider.DistancePrices[_upgradesProvider.DistanceLevelModel].Value;
                    if (_moneyProvider.Money >= value)
                    {
                        _moneyProvider.Money -= value;
                        _upgradesProvider.DistanceLevelModel++;
                        return true;
                    }
                    break;
                default:
                    Debug.LogWarning("Default upgrade type");
                    break;
            }
            return false;
        }
    }
}