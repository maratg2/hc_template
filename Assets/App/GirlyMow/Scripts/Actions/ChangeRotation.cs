using UnityEngine;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Components;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ChangeRotation : CoreAction
    {
        [InjectComponent] private ScriptableAxis2DDecorator _axis2DDecorator;
        [InjectComponent] private MeshTransformProvider _meshTransformProvider;
        protected override bool Action()
        {
            Vector3 axisDir = new Vector3(_axis2DDecorator.Axis.x, 0f, _axis2DDecorator.Axis.y);
            Vector3 direction = Vector3.RotateTowards(_meshTransformProvider.transform.right,
                axisDir * 1000f, 10000f, 10000f);
            if(direction != Vector3.zero)
            {
                _meshTransformProvider.meshTransform.rotation = Quaternion.LookRotation(direction);
                return true;
            }
            else
                return false;
        }
    }
}