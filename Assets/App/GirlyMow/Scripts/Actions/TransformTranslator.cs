using UnityEngine;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Components;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class TransformTranslator : CoreAction
    {
        [InjectComponent] private TransformMovable _transformMovable;
        public InputData<float> deltaTime;
        protected override bool Action()
        {
            float speedMulti = 1f;
            if (PlayerPrefs.HasKey("SpeedMulti"))
                speedMulti = PlayerPrefs.GetFloat("SpeedMulti");

            Vector3 movementVector =
                new Vector3(_transformMovable.InputAxis.Axis.x, 0f, _transformMovable.InputAxis.Axis.y);
            _transformMovable.transform.Translate(movementVector * _transformMovable.sensitivity * deltaTime.Data * speedMulti);
            return true;
        }
    }
}