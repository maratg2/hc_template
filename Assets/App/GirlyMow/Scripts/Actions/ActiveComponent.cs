using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ActiveComponent : CoreAction
    {
        [ActionParameter] public bool state;
        public InputComponent<ComponentProvider> componentProvider;

        protected override bool Action()
        {
            //Insert Action Code Here
            componentProvider.Component.component.enabled = state;
            return true;
        }
    }
}