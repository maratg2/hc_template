using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ChangeTimeMulti : CoreAction
    {
        private bool _isTriggered;
        protected override bool Action()
        {
            if (_isTriggered)
                return false;
            
            Time.timeScale = 0.9f;
            _isTriggered = true;
            return true;
        }
    }
}