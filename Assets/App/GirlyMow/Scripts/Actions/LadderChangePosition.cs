using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components.Links;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class LadderChangePosition : CoreAction
    {
        [InjectComponent] private UpgradesProvider _upgradesProvider;
        [InjectComponent] private TransformLink _transform;
        protected override bool Action()
        {
            _transform.Transform.localPosition = new Vector3
                (0f,
                 8.5f - 2f * 10f - 2f * 3f * _upgradesProvider.DistanceLevelModel,
                   1f + 1.75f * 10f + 1.75f * 3f * _upgradesProvider.DistanceLevelModel);
            return true;
        }
    }
}