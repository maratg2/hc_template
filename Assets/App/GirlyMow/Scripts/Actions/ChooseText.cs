using System;
using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components.Links;
using Krem.AppCore.Ports;
using TMPro;
using UnityEngine;


namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ChooseText : CoreAction
    {
        [ActionParameter] public float amountMulti = 0.2f;
        [ActionParameter] public int amountMoney = 100;

        public InputComponent<PositionProvider> positionProvider;
        [InjectComponent] private TransformLink _transform;
        [InjectComponent] private LadderTextProvider _ladderTextProvider;
        [InjectComponent] private MoneyProvider _moneyProvider;
        public TextMeshPro tmp;
        private int oldIndex = -1;
        private float color = 0f;
        
        protected override bool Action()
        {
            tmp = _ladderTextProvider.Texts[0];
            Vector3 posDoll = positionProvider.Component.tTransform.position;
            Vector3 pos = _transform.Transform.position;
            if (posDoll.z > 0.4f + pos.z)
            {
                int textIndex = 0;
                while (posDoll.z > _ladderTextProvider.Texts[textIndex].transform.position.z + 0.8f)
                    textIndex++;
                
                if (textIndex != oldIndex && textIndex <= _ladderTextProvider.Texts.Count - 1)
                {
                    _moneyProvider.MoneyLvl += (int)(amountMoney * amountMulti);
                    _moneyProvider.Money += (int)(amountMoney * amountMulti);
                    _moneyProvider.MoneyLadder += (int)(amountMoney * amountMulti);
                    AudioSource.PlayClipAtPoint(_ladderTextProvider.CoinAudio, pos);
                    tmp = _ladderTextProvider.Texts[textIndex];
                    ChangeTMP();
                    oldIndex = textIndex;
                    return true;
                }
                if(textIndex > _ladderTextProvider.Texts.Count - 1)
                    Debug.LogWarning("Doll is outside of bounds of ladder");
                
                return false;
            }
            return false;
        }

        private void ChangeTMP()
        {
            MeshRenderer[] renderers = tmp.transform.parent.GetComponentsInChildren<MeshRenderer>();
            tmp.enabled = true;
            renderers[0].material.color = Color.HSVToRGB(Mathf.PingPong(color, 1), 1, 1);
            tmp.outlineColor = renderers[0].material.color;
            tmp.color = Color.white;
            renderers[1].material.color = renderers[0].material.color;
            color += 0.01f;
        }
    }
}