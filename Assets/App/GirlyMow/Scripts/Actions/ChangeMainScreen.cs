using System;
using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class ChangeMainScreen : CoreAction
    {
        public InputComponent<MainPanelProvider> _mainPanelProvider;
        [ActionParameter] public bool activate;
        protected override bool Action()
        {
            _mainPanelProvider.Component.canvasGroup.alpha = Convert.ToSingle(activate);
            _mainPanelProvider.Component.canvasGroup.interactable = activate;
            _mainPanelProvider.Component.canvasGroup.blocksRaycasts = activate;
            return true;
        }
    }
}