using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class TimelineTurn : CoreAction
    {
        [InjectComponent] private TimelineProvider _timelineProvider;
        [ActionParameter] public bool turnOn = true;

        protected override bool Action()
        {
            if (turnOn)
                _timelineProvider.playableDirector.Play();
            else
                _timelineProvider.playableDirector.Stop();
            return true;
        }
    }
}