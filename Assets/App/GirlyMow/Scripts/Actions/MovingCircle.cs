using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("HW1")] 
    public class MovingCircle : CoreAction
    {
        public InputComponent<Force> force;
        public InputComponent<Rotation> rotation;
        [InjectComponent] private Rigidbody _rb;
        [InjectComponent] private Transform _transform;
        protected override bool Action()
        {
            //Insert Action Code Here
            Rotate();
            MoveCircle();
            return true;
        }

        private void Rotate()
        {
            _rb.rotation *= Quaternion.Euler(rotation.Component.RotationValue);
        }
        private void MoveCircle()
        {
            _rb.velocity = _rb.rotation * force.Component.ForceValue;
        }
    }
}
