using App.GirlyMow.States;
using Krem.AppCore;
using Krem.AppCore.Attributes;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]

    public class GameStateIs : CoreAction
    {
        [InjectComponent] private GameStateProvider _gameStateProvider;
        [ActionParameter] public GameState gameState;
        [ActionParameter] public bool isNot;
        protected override bool Action()
        {
            return isNot ? _gameStateProvider.scriptableGameState.Model.State != gameState :
                           _gameStateProvider.scriptableGameState.Model.State == gameState;
        }
    }
}