using App.GirlyMow.States;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]

    public class ChangeGameStateTo : CoreAction
    {
        [InjectComponent] private GameStateProvider _gameStateProvider;
        [ActionParameter] public GameState gameState;
        protected override bool Action()
        {
            Debug.Log(gameState);
            _gameStateProvider.scriptableGameState.Model.State = gameState;
            return true;
        }
    }
}
