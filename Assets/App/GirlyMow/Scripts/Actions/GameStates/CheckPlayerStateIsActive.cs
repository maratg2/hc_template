using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]

    public class CheckPlayerStateIsActive : CoreAction
    {
        [InjectComponent] private Player _player;
        [ActionParameter] public bool isNot;
        protected override bool Action()
        {
            return isNot ? !_player.isActive :
                            _player.isActive;
        }
    }
}