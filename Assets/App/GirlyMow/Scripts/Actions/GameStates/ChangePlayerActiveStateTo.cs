using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]

    public class ChangePlayerActiveStateTo : CoreAction
    {
        [InjectComponent] private Player _player;
        [ActionParameter] public bool state;
        protected override bool Action()
        {
            _player.isActive = state;
            return true;
        }
    }
}