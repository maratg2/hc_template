using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour
{
    [SerializeField] private float _speed = 5f;
    void Update()
    {
        transform.Rotate(Vector3.forward, _speed * Time.deltaTime, Space.Self);
    }
}
