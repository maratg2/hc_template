using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceStart : MonoBehaviour
{
    public float force = 100f;
    public bool isMain;
    public Animator _girlAnimator;
    private Vector3 _oldPos = Vector3.zero;
    private bool _was;
    public bool isSecond = false;

    void Awake()
    {
        GetComponent<Rigidbody>().AddForce(new Vector3(0f, 1f, 1f) * force, ForceMode.Impulse);
    }

    private void FixedUpdate()
    {
        if (!isMain)
            return;
        
        if ((_oldPos - transform.position).sqrMagnitude < 0.000005f && !_was)
        {
            Debug.Log("Next");
            _girlAnimator.gameObject.SetActive(true);
            _girlAnimator.SetTrigger("Start");
            _was = true;
            if(isSecond)
                transform.parent.parent.gameObject.SetActive(false);
            else
                transform.parent.gameObject.SetActive(false);
        }
        _oldPos = transform.position;
    }
}
