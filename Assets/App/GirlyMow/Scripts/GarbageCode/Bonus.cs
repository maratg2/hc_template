using System;
using System.Collections;
using System.Collections.Generic;
using App.GirlyMow.Components;
using UnityEngine;
using Random = UnityEngine.Random;

public enum BonusType
{
    Size,
    Speed,
    Money,
    Food
}

public class Bonus : MonoBehaviour
{
    [SerializeField] private AudioClip _coinSound;
    [SerializeField] private AudioClip _bonusSound;
    [SerializeField] private GameObject[] _foodList;
    public BonusType bonusType;
    public ParticleSystem bonusTakeVFX;
    public Collider triggerCollider;
    public Collider collider;
    public ImageMoney imageMoney;
    public float spawnForce = 500f;
    private float oldPosY = 0f;
    private void Awake()
    {
        Spawn();
        if (bonusType == BonusType.Food)
            StartCoroutine(LateTake(2f));
        if (bonusType == BonusType.Money)
            StartCoroutine(LateTake(1.3f));
    }

    private void Spawn()
    {
        if(bonusType == BonusType.Food)
            _foodList[Mathf.FloorToInt(Random.Range(0f, _foodList.Length - 0.0001f))].SetActive(true);
        
        transform.rotation *= Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
        
        Rigidbody rb = GetComponent<Rigidbody>();
        Vector3 dirVector3 = new Vector3(0.5f, 1f, 0.5f);
        
        if (Random.Range(0f, 1f) <= 0.5f)
            dirVector3.x = 0.5f + Random.Range(0f, 0.3f);
        else
            dirVector3.x = -0.5f - Random.Range(0f, 0.3f);
        
        if (Random.Range(0f, 1f) <= 0.5f)
            dirVector3.z = 0.5f + Random.Range(0f, 0.3f);
        else
            dirVector3.z = -0.5f - Random.Range(0f, 0.3f);
        
        rb.AddForce(dirVector3 * spawnForce);
    }

    private void Update()
    {
        if (transform.position.y <= 1f && oldPosY > 1f)
        {
            collider.enabled = true;
            triggerCollider.enabled = true;
        }
        oldPosY = transform.position.y;
    }

    private IEnumerator LateTake(float timeDelay)
    {
        yield return new WaitForSecondsRealtime(timeDelay);
        if(bonusType == BonusType.Food)
            FindObjectOfType<TriggerBonusProvider>().TriggerFood();
        EmulateDestroy(true);
    }
    
    public void EmulateDestroy(bool toDestroy)
    {
        if(toDestroy)
        {
            if (bonusType == BonusType.Money)
            {
                Vector2 posScreen = Camera.main.WorldToScreenPoint(transform.position);
                ImageMoney image = Instantiate(imageMoney, posScreen, Quaternion.identity, FindObjectOfType<PanelInGame>().transform);
                image.GetComponent<RectTransform>().anchoredPosition = posScreen;
            }
            ParticleSystem takeVFX = Instantiate(bonusTakeVFX, transform.position, Quaternion.identity);
            if (bonusType == BonusType.Money)
            {
                takeVFX.transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                AudioSource.PlayClipAtPoint(_coinSound, transform.position);
            }
            else
            {
                takeVFX.transform.localScale = new Vector3(1.15f, 1.15f, 1.15f);
                AudioSource.PlayClipAtPoint(_bonusSound, transform.position);
            }
            
            gameObject.SetActive(false);
        }
    }
}
