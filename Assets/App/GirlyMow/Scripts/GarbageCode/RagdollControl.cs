using System;
using System.Collections;
using App.GirlyMow.States;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Actions;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]

    public class RagdollControl : CoreComponent
    {
        public OutputSignal OnDollStopped;
        public bool isIgnore = false;
        [SerializeField] private Animator _animator;
        [SerializeField] private Rigidbody[] _rbds;
        [SerializeField] private Rigidbody _mainRb;
        [SerializeField] private Vector3 _dir;
        [SerializeField] private ParticleSystem _hitButtFX;
        [SerializeField] private Vector3 _hitOffset;
        [SerializeField] private float multi = 1f;
        
        private Vector3 _oldPos;
        private bool _IsStarted = false;
        private bool _ToSave = false;
        private UpgradesProvider _upgradesProvider;
        
        private void FixedUpdate()
        {
            if (!_IsStarted)
                return;
            
            _ToSave = !_ToSave;
            if (_ToSave)
                _oldPos = _rbds[1].transform.position;
            else if (Mathf.Abs(_oldPos.z - _rbds[1].transform.position.z) < 0.0001f)
            {
                OnDollStopped?.Invoke();
            }
        }       

        private void Awake()
        {
            _upgradesProvider = GetComponent<UpgradesProvider>();
            if(!isIgnore)
            {
                _animator.enabled = true;
                foreach (var rb in _rbds)
                    rb.isKinematic = true;
            }
            else
            {
                _animator.enabled = false;
                foreach (var rb in _rbds)
                    rb.isKinematic = false;
            }
        }

        public void MakePhysical(float power)
        {
            ParticleSystem newHitButtFX = Instantiate(_hitButtFX, transform.position + _hitOffset, Quaternion.identity);
            newHitButtFX.Play();
            
            _animator.enabled = false;
            foreach (var rb in _rbds)
                rb.isKinematic = false;
            _mainRb.AddForce(_dir * power * multi * (1f + 0.025f * (1 + _upgradesProvider.ButtLevelModel)), ForceMode.Impulse);
            _IsStarted = true;
        }
    }
}