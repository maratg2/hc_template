using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageMoney : MonoBehaviour
{
    public float seconds = 1f;
    private Vector2 _startPos;
    private Vector2 _endPos;
    private Image _image;
    private RectTransform _rect;
    
    private void Start()
    {
        _image = GetComponent<Image>();
        _rect = GetComponent<RectTransform>();
        _startPos = _rect.anchoredPosition;
        _rect.anchoredPosition3D = new Vector3(_rect.anchoredPosition.x, _rect.anchoredPosition.y, 0f);
        _endPos = new Vector2(Screen.width - Screen.width / 15f, Screen.height - Screen.height / 12f) * 1920f / Screen.height;
        StartCoroutine(SmoothMove());
    }

    private IEnumerator SmoothMove()
    {
        float t = 0f;
        while (t <= 1f)
        {
            t += Time.deltaTime / seconds;
            Vector2 newPos = Vector2.Lerp(_startPos, _endPos, Mathf.SmoothStep(0f, 1f, t));
            _rect.anchoredPosition = newPos;

            if (t >= 0.5f)
                _image.color = new Color(1f, 1f, 1f, 1f - 2f * (t - 0.5f));
            
            yield return new WaitForEndOfFrame();
        }
    }
}
