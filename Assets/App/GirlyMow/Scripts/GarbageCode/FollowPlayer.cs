using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private Transform _mowerTransform;
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Vector3 _offset;
    private void Update()
    {
        transform.position = new Vector3(_playerTransform.position.x + _offset.x, _offset.y, _mowerTransform.position.z + _offset.z);
    }
}
 