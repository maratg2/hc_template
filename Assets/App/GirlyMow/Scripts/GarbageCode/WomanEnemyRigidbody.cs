using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WomanEnemyRigidbody : MonoBehaviour
{
    [SerializeField] private ParticleSystem _hitVFX;
    [SerializeField] private AudioClip _hitSFX;
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision other)
    {
        Ladder otherLadder = other.gameObject.GetComponent<Ladder>();
        if (!otherLadder)
            return;

        ParticleSystem newHitFX = Instantiate(_hitVFX, other.GetContact(0).point, Quaternion.identity);
        newHitFX.Play();
        _audioSource.PlayOneShot(_hitSFX);
    }
}
