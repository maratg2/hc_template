using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfAnimation : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Sprite[] _animationSprites;
    [SerializeField] private float _timeFrame = 0.04f;
    
    private bool _isPlaying;
    private float _timer = 0f;
    private int _frameIndex = 0;

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > _timeFrame)
        {
            _timer = 0f;
            _image.sprite = _animationSprites[_frameIndex];
            _frameIndex++;
            if (_frameIndex >= _animationSprites.Length)
                _frameIndex = 0;
        }
    }
    private void OnEnable()
    {
        _isPlaying = true;
    }

    private void OnDisable()
    {
        _isPlaying = false;
    }
}
