using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBeforeBridge : MonoBehaviour
{
    [SerializeField] private Vector3 _endPos;
    private Vector3 _startPos;
    private bool _isInCR;
    
    public void OnBeginFollow(float time)
    {
        if (!_isInCR)
            StartCoroutine(Follow(time));
    }

    private IEnumerator Follow(float time)
    {
        _isInCR = true;
        float t = 0f;
        _startPos = transform.position;
        while (t < 1f)
        {
            transform.position = Vector3.Lerp(_startPos, _endPos, t);
            t += Time.deltaTime / time;
            yield return new WaitForEndOfFrame();
        }
    }
}
