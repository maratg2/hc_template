using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarProgress : MonoBehaviour
{
    [SerializeField] private Image _barImage;
    [SerializeField] private Image _barIconImage;
    private bool _isInc;
    public void UpdateBarImage(float fillAmount)
    {
        _barImage.fillAmount = fillAmount;
    }

    public void Update()
    {
        if (_barImage.fillAmount >= 0.5f)
            _barIconImage.color = new Color(0.2f + (_barImage.fillAmount - 0.5f) * 2f, 0.2f+ (_barImage.fillAmount - 0.5f) * 2f, 0.2f+ (_barImage.fillAmount - 0.5f) * 2f, 1f);
        if (_barImage.fillAmount >= 0.95f)
        {
            if (_isInc)
            {
                _barIconImage.rectTransform.localScale += new Vector3(Time.unscaledDeltaTime, Time.unscaledDeltaTime, Time.unscaledDeltaTime);
                if (_barIconImage.rectTransform.localScale.x > 1.15f)
                    _isInc = !_isInc;
            }
            else
            {
                _barIconImage.rectTransform.localScale -= new Vector3(Time.unscaledDeltaTime, Time.unscaledDeltaTime, Time.unscaledDeltaTime);
                if (_barIconImage.rectTransform.localScale.x < 0.85f)
                    _isInc = !_isInc;
            }
        }
    }
}
