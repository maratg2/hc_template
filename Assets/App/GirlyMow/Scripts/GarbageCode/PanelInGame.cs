using System;
using System.Collections;
using System.Collections.Generic;
using App.GirlyMow.States;
using Krem.Core.Packages.ScriptableORM.Models;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelInGame : MonoBehaviour
{
    [SerializeField] private IntScriptableModel _moneyCurrent;
    [SerializeField] private ScriptableGameState _gameState;
    void Start()
    {
        TinySauce.OnGameStarted(SceneManager.GetActiveScene().name);
    }

    private void OnDestroy()
    {
        bool isUserCompleteLevel = _gameState.Model.State == GameState.Win;
        float score = _moneyCurrent.Model.Value;
        TinySauce.OnGameFinished(isUserCompleteLevel, score, SceneManager.GetActiveScene().name);
    }
}
