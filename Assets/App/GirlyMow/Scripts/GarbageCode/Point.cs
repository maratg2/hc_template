using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    public Transform setTransform;
    
    void FixedUpdate()
    {
        transform.position = setTransform.position;
    }
}
