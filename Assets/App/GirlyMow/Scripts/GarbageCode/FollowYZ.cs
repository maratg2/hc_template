using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowYZ : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Vector3 _offset;
    private void Update()
    {
        transform.position = new Vector3(_offset.x, _playerTransform.position.y + _offset.y, _playerTransform.position.z + _offset.z);
    }
}
