using System.Collections;
using System.Collections.Generic;
using App.GirlyMow.Components;
using UnityEngine;

public class Grass : MonoBehaviour
{
    [SerializeField] private AudioClip _cutSound;
    [SerializeField] private GameObject _grass1;
    [SerializeField] private GameObject _grass2;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private GameObject[] _bonusPrefabs;
    [SerializeField] private GameObject _moneyPrefab;
    [SerializeField] private GameObject _foodPrefab;
    [SerializeField] private float _time = 1f;
    [SerializeField] private int _grassCount = 255;
    [SerializeField] private int _remainFood = 20;
    [SerializeField] private int _maxGrassPerFood = 20;
    [SerializeField] private float _probabilityFood = 0.05f;
    public bool destroyed;
    private bool isInVacuum;
    
    
    private void Awake()
    {
        PlayerPrefs.SetInt("Bonus1", 0);
        PlayerPrefs.SetInt("Bonus2", 0);
        PlayerPrefs.SetInt("Food", 0);
        PlayerPrefs.SetInt("RemainFood", _remainFood);
        PlayerPrefs.SetInt("Grass", _grassCount);
    }
    public void EmulateDestroy(bool toDestroy, Transform parent = null)
    {
        if(toDestroy && _grass1.activeSelf)
        {
            AudioSource.PlayClipAtPoint(_cutSound, transform.position);
            int grassC = PlayerPrefs.GetInt("Grass");
            //Debug.Log(grassC);
            PlayerPrefs.SetInt("Grass", -1 + grassC);
            if (grassC == 1)
            {
                FindObjectOfType<AssProvider>().ChangeAss();
                Grass[] grasses = FindObjectsOfType<Grass>();
                foreach (var grass in grasses)
                    if(grass != this)
                        grass.gameObject.SetActive(false);
            }
            RandomSpawn();
            destroyed = true;
            if (_particleSystem)
            {
                _particleSystem.startColor = _grass1.GetComponent<MeshRenderer>().material.GetColor("_ColorTop");
                _particleSystem.Play();
            }
        }
        if(!parent)
        {
            _grass1.SetActive(!toDestroy);
            _grass2.SetActive(!toDestroy);
        }
        else
        {
            if (!isInVacuum)
            {
                StartCoroutine(Suction(parent));
                isInVacuum = true;
            }
        }
    }

    private IEnumerator Suction(Transform parent)
    {
        transform.parent = parent;
        float t = 0.0f;
        float oldT = -1f;
        bool startedScale = false;
        Vector3 cachedLocalScale = transform.localScale;
        Vector3 cachedPos = transform.localPosition;
        while (t < 1f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, t);
            t += Time.deltaTime / _time;
            if (oldT < 0.2f && t >= 0.2f)
                startedScale = true;

            if (startedScale)
            {
                float normalT = (t - 0.2f) * 10f;
                if (normalT > 1f)
                {
                    normalT = 1f;
                    startedScale = false;
                }
                transform.localScale = cachedLocalScale * (1 - normalT);
            }
            
            oldT = t;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        transform.localPosition = Vector3.zero;
    }

    private void RandomSpawn()
    {
        int curFood = PlayerPrefs.GetInt("Food");
        int remainFood = PlayerPrefs.GetInt("RemainFood");
        PlayerPrefs.SetInt("Food", 1 + curFood);
        
        if (Random.Range(0f, 1f) <= 0.75f)
        {
            if (Random.Range(0f, 1f) <= 0.9f)
                SpawnMoney();
            if (Random.Range(0f, 1f) <= 0.5f)
                SpawnMoney();
            if (Random.Range(0f, 1f) <= 0.3f)
                SpawnMoney();
        }
        else
            SpawnBonus();

        if (curFood > _maxGrassPerFood && remainFood > 0)
        {
            SpawnFood();
            PlayerPrefs.SetInt("Food", 0);
            PlayerPrefs.SetInt("RemainFood", -1 + remainFood);
        }
        else if(remainFood > 0)
        {
            if(Random.Range(0f, 1f) <= _probabilityFood)
            {
                SpawnFood();
                PlayerPrefs.SetInt("RemainFood", -1 + remainFood);
            }
        }
    }

    private void SpawnBonus()
    {
        int index = Mathf.FloorToInt(Random.Range(0f, 1.999f));
        if (PlayerPrefs.GetInt("Bonus" + (index + 1)) == 1)
            return;

        Vector3 newPosition = new Vector3(transform.position.x, 0.5f, transform.position.z);
        GameObject newbonus = Instantiate(_bonusPrefabs[index], newPosition, Quaternion.identity);
        PlayerPrefs.SetInt("Bonus" + (index+1), 1);
    }

    private void SpawnMoney()
    {
        if (!_moneyPrefab)
            return;
        
        Vector3 newPosition = new Vector3(transform.position.x, 0.5f, transform.position.z);
        GameObject newMoney = Instantiate(_moneyPrefab, newPosition, Quaternion.identity);
    }
    private void SpawnFood()
    {
        if (!_foodPrefab)
            return;
        Vector3 newPosition = new Vector3(transform.position.x, 0.5f, transform.position.z);
        GameObject newFood = Instantiate(_foodPrefab, newPosition, Quaternion.identity);
    }
}
