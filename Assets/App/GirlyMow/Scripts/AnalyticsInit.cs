using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;

public class AnalyticsInit : MonoBehaviour
{
    private void Awake()
    {
        GameAnalytics.Initialize();
    }
}
