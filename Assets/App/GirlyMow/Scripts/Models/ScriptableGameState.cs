using System.Diagnostics.CodeAnalysis;
using Krem.Core.Packages.ScriptableORM;
using UnityEngine;

namespace App.GirlyMow.States
{
    [CreateAssetMenu(fileName = "ScriptableGameState", menuName = "GirlyMow/States/ScriptableGameState", order = 0)]
    public class ScriptableGameState : ScriptableModel<GameStateModel>
    {

    }
}