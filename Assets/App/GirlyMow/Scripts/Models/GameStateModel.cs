using System;
using Krem.Core.Packages.ScriptableORM;
using UnityEngine;

namespace App.GirlyMow.States
{
    public enum GameState
    {
        WaitForStart,
        Started,
        LevelEnd,
        Win,
        Lose
    }
    
    [Serializable]
    public class GameStateModel : Model
    {
        [SerializeField] protected GameState _state;
        
        public GameState State { get => _state; set => SetField(ref _state, value); }
    }
}