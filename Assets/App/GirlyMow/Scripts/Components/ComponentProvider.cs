﻿using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class ComponentProvider : CoreComponent
    {
        [NotNull] public Behaviour component;
        public OutputComponent compOut;
    }
}