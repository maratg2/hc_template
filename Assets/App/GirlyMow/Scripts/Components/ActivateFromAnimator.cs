﻿using System;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class ActivateFromAnimator : CoreComponent
    {
        [SerializeField, NotNull] private CanvasGroup _canvasGroup;
        
        public void Activate()
        {
            if(Application.isPlaying)
            {
                Time.timeScale = 0.05f;
                ActivateBool(true);
            }
        }

        public void ArrowForce()
        {
            if(Time.timeScale < 0.1f)
                Time.timeScale = 0.9f;
            ActivateBool(false);
        }

        private void ActivateBool(bool activate)
        {
            _canvasGroup.alpha = Convert.ToSingle(activate);
            _canvasGroup.interactable = activate;
            _canvasGroup.blocksRaycasts = activate;
        }
    }
}