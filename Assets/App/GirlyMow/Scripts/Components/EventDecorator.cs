﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]

    [DisallowMultipleComponent]
    public class EventDecorator : CoreComponent
    {
        [Header("Ports")] public OutputSignal OnHandle;
        public void CallHandle() { OnHandle?.Invoke(); }
    }
}