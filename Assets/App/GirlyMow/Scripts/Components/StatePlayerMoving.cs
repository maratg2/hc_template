﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class StatePlayerMoving : CoreComponent
    {
        public OutputData<bool> _isMoving;
        
        public bool isMoving { get => isMoving; set => _isMoving.Data = value; }
    }
}