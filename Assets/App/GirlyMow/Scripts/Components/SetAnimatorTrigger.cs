﻿using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class SetAnimatorTrigger : CoreAction
    {
        [InjectComponent] private AnimatorProvider _animatorProvider;
        [ActionParameter] public string triggerStateName;

        protected override bool Action()
        {
            _animatorProvider.animator.SetTrigger(triggerStateName);
            return true;
        }
    }
}