﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using App.GirlyMow.States;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.States
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class GameStateProvider : CoreComponent
    {
        [Header("Ports")] 
        public OutputSignal OnModelChanged;
        
        [Header("Dependencies")] [SerializeField, NotNull]
        protected ScriptableGameState _scriptableGameState;
        
        public ScriptableGameState scriptableGameState => _scriptableGameState;
        
        
        void OnEnable()  { _scriptableGameState.Model.PropertyChanged += ModelChangedHandler; }
        void OnDisable() { _scriptableGameState.Model.PropertyChanged -= ModelChangedHandler; }
        
        private void ModelChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            OnModelChanged?.Invoke();
        }
    }
}