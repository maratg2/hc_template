﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("HW1")]
    [DisallowMultipleComponent]
    public class Rotation : CoreComponent
    {
        [SerializeField] private Vector3 _rotation;

        public Vector3 RotationValue
        {
            get => _rotation;
            set
            {
                _rotation = value;
            }
        }
    }
}