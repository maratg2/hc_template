﻿using System;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.Core.Packages.ScriptableORM.Models;
using UnityEngine;


namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class MoneyProvider : CoreComponent
    {
        [SerializeField, NotNull] protected IntScriptableModel _moneyModel;
        [SerializeField, NotNull] protected IntScriptableModel _moneyLvlModel;
        [SerializeField, NotNull] protected IntScriptableModel _moneyLadderModel;
        
        private void Awake()
        {
            MoneyLvl = 0;
            Time.timeScale = 1f;
        }

        public int Money
        {
            get => _moneyModel.Model.Value;
            set => _moneyModel.Model.Value = value;
        }

        public int MoneyLvl
        {
            get => _moneyLvlModel.Model.Value; 
            set => _moneyLvlModel.Model.Value = value;
        }
        
        public int MoneyLadder
        {
            get => _moneyLadderModel.Model.Value; 
            set => _moneyLadderModel.Model.Value = value;
        }
    }
}