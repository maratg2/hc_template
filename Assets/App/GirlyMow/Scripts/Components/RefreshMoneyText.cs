using System.Diagnostics.CodeAnalysis;
using App.GirlyMow.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using Krem.Core.Packages.ScriptableORM.Models;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class RefreshMoneyText : CoreAction
    {
        [InjectComponent] private MoneyProvider _moneyProvider;
        public InputComponent<TMPLink> tmpLink;
        
        protected override bool Action()
        {
            tmpLink.Component.Text.text = _moneyProvider.Money.ToString();
            return true;
        }
    }
}