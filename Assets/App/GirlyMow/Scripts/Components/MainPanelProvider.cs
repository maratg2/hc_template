﻿using System;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class MainPanelProvider : CoreComponent
    {
        [Header("Dependencies")] [SerializeField, NotNull] protected CanvasGroup _canvasGroup;

        public CanvasGroup canvasGroup
        {
            get => _canvasGroup;
        }
    }
}