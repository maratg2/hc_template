﻿using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using TMPro;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class TMPLink : CoreComponent
    {
        [SerializeField, NotNull] protected TextMeshProUGUI _tmpText;

        public TextMeshProUGUI Text => _tmpText;
    }
}