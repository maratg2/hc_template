﻿using System;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class TriggerGrassProvider : CoreComponent
    {
        public bool isFood = false;
        public Transform parentForFood;
        public OutputSignal OnTrig;
        public OutputData<Collider> grassCollider;
        public static event Action OnGrassDestroyed;
        private void OnTriggerEnter(Collider other)
        {
            Grass otherGrass = other.GetComponent<Grass>();
            if (otherGrass && !otherGrass.destroyed)
            {
                //grassCollider.Data = other;
                OnGrassDestroyed?.Invoke();
                if(isFood)
                    otherGrass.EmulateDestroy(true, parentForFood);
                else
                    otherGrass.EmulateDestroy(true);
                OnTrig?.Invoke();
            }
        }
    }
}