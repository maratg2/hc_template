﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]

    [DisallowMultipleComponent]
    public class Player : CoreComponent
    {
        public AudioClip _hitAudio;
        public bool isActive { get; set; } = false;

        public void SetBoolMoving()
        {
            GetComponentInChildren<AssProvider>().GetComponent<Animator>().SetBool("IsMoving", false);
        }

        public void RagdollOn(float power)
        {
            float multi = 1f;
            if (Time.timeScale > 1f)
                multi = 100f * (Time.timeScale - 1f);
            multi += 4;
            multi *= 10;
            multi = Mathf.Log(multi);
            AudioSource.PlayClipAtPoint(_hitAudio, transform.position);
            Time.timeScale = 1f;
            FindObjectOfType<RagdollControl>().MakePhysical(power * multi);
        }
    }
}