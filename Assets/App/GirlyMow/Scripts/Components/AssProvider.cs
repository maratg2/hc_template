﻿using System;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using Krem.Core.Packages.ScriptableORM.Models;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class AssProvider : CoreComponent
    {
        [SerializeField, NotNull] protected Transform _leftHalf, _rightHalf;
        [SerializeField, NotNull] protected Transform _ass;
        [SerializeField, NotNull] protected FloatScriptableModel _assModel;
        
        public OutputSignal OnAssChanged;
        public OutputData<float> assScale;

        private void OnEnable()
        {
            TriggerBonusProvider.OnFoodTaken += ChangeAss;
        }
        private void OnDisable()
        {
            TriggerBonusProvider.OnFoodTaken -= ChangeAss;
        }

        public void ChangeAss()
        {
            OnAssChanged?.Invoke();
        }

        public void Update()
        {
            float scale = 1;
            if (Single.TryParse(_assModel.Model.Value, out scale))
            {
                assScale.Data = scale;
            }
            else if (!assScale.Data.Equals(null) && !assScale.Data.Equals(1f))
                assScale.Data = 1;
        }

        public Transform LeftHalf
        {
            get => _leftHalf;
        }

        public Transform RightHalf
        {
            get => _rightHalf;
        }
        
        public Transform Ass
        {
            get => _ass;
        }
    }
}