﻿using System;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components.Links;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class ArrowMultiProvider : CoreComponent
    {
        public InputComponent<TransformLink> arrowTransform;
        public OutputSignal OnMultiCalculated;
        public OutputSignal OnEnd;
        public OutputData<int> multiOut;
        private float _angleZ = 34f;
        private bool _isRight;
            
        private void Update()
        {
            if (arrowTransform.Component.Transform.localRotation.z > _angleZ / 100f)
                _isRight = false;
            else if (arrowTransform.Component.Transform.localRotation.z < -_angleZ / 100f)
                _isRight = true;
            
            float rotAbsZ = Mathf.Abs(arrowTransform.Component.Transform.localEulerAngles.z);
            if (rotAbsZ > 180f)
                rotAbsZ = rotAbsZ - 360f;
            
            if(_isRight)
            {
                if (rotAbsZ is <= 4.1f and >= -1f)
                    multiOut.Data = 6;
                else if (rotAbsZ is <= 11.8f and >= -8.7f)
                    multiOut.Data = 5;
                else if (rotAbsZ is <= 18.5f and >= -15.2f)
                    multiOut.Data = 4;
                else if (rotAbsZ is <= 25.5f and >= -22.5f)
                    multiOut.Data = 3;
                else if (rotAbsZ is <= 33.5f and >= -30.3f)
                    multiOut.Data = 2;
                else
                    multiOut.Data = 1;
            }
            else
            {
                if (rotAbsZ is <= 2.3f and >= -4.5f)
                    multiOut.Data = 6;
                else if (rotAbsZ is <= 8.4f and >= -10.7f)
                    multiOut.Data = 5;
                else if (rotAbsZ is <= 15f and >= -18.6f)
                    multiOut.Data = 4;
                else if (rotAbsZ is <= 22.5f and >= -25.5f)
                    multiOut.Data = 3;
                else if (rotAbsZ is <= 30.5f and >= -33.5f)
                    multiOut.Data = 2;
                else
                    multiOut.Data = 1;
            }
            
            if(Time.timeScale < 0.1f)
                OnMultiCalculated?.Invoke();
            else if (Time.timeScale <= 0.9f)
            {
                Time.timeScale = 1f + multiOut.Data / 100f;
                OnEnd?.Invoke();
            }
        }
    }
}