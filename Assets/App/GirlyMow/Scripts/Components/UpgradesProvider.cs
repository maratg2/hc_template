﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.Core.Packages.ScriptableORM.Models;
using UnityEngine;

namespace App.GirlyMow.Components
{
    public enum UpgradeType
    {
        Butt,
        Distance
    }
    
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class UpgradesProvider : CoreComponent
    {
        [SerializeField, NotNull] protected IntScriptableList _buttPricesList;
        [SerializeField, NotNull] protected IntScriptableModel _buttLevelModel;
        [SerializeField, NotNull] protected IntScriptableList _distancePricesList;
        [SerializeField, NotNull] protected IntScriptableModel _distanceLevelModel;
        
        public List<IntModel> ButtPrices { get => _buttPricesList.Collection; }
        public int ButtLevelModel
        {
            get => _buttLevelModel.Model.Value;
            set => _buttLevelModel.Model.Value = value;
        }
        
        public List<IntModel> DistancePrices { get => _distancePricesList.Collection; }
        public int DistanceLevelModel
        {
            get => _distanceLevelModel.Model.Value;
            set => _distanceLevelModel.Model.Value = value;
        }
    }
}