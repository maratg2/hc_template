﻿using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class AnimatorProvider : CoreComponent
    {
        [Header("Dependencies")] [SerializeField, NotNull]
        protected Animator _animator;
        
        public Animator animator
        {
            get => _animator;
        }
    }
}