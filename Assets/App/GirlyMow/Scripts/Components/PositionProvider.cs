﻿using System;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class PositionProvider : CoreComponent
    {
        [SerializeField, NotNull] protected Transform _targetTransform;
        public Transform tTransform => _targetTransform;
    }
}