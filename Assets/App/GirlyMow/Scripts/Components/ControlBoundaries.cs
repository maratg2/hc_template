using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Components;
using Krem.AppCore.Ports;
using UnityEngine;

[NodeGraphGroupName("Girly Mow")] 
public class ControlBoundaries : CoreAction
{
    [InjectComponent] private Transform _transformComponent;
    [InjectComponent] private TransformMovable _transformMovable;
    [ActionParameter] public Vector3 xBound;
    [ActionParameter] public Vector3 zBound;
    protected override bool Action()
    {
        Vector3 newPos = _transformComponent.position + new Vector3(_transformMovable.InputAxis.Axis.x, 0, _transformMovable.InputAxis.Axis.y) / 5;

        if (newPos.x > xBound.x && newPos.x < xBound.z &&
            newPos.z > zBound.x && newPos.z < zBound.z)
            return true;
        else
            return false;
    }
}
