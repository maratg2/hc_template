﻿using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class MeshTransformProvider : CoreComponent
    {
        [SerializeField, NotNull] public Transform meshTransform;
    }
}