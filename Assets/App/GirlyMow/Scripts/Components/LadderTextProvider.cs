﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using TMPro;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class LadderTextProvider : CoreComponent
    {
        [SerializeField, NotNull] private List<TextMeshPro> _texts;
        [SerializeField, NotNull] private AudioClip _coinAudio;
        public List<TextMeshPro> Texts => _texts;
        public AudioClip CoinAudio => _coinAudio;
    }
}
