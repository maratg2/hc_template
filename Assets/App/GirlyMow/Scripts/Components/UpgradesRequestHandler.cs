﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class UpgradesRequestHandler : CoreComponent
    {
        public OutputSignal onButtRequest, onDistanceRequest, onFreeRequest, onStoreRequest;
        
        public void RequestButtUpgrade()
        {
            onButtRequest?.Invoke();
        }

        public void RequestDistanceUpgrade()
        {
            onDistanceRequest?.Invoke();
        }

        public void RequestFreeMoney()
        {
            onFreeRequest?.Invoke();
        }

        public void RequestStore()
        {
            onStoreRequest?.Invoke();
        }
    }
}