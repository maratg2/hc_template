﻿using System;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class OutputBool : CoreComponent
    {
        public OutputData<bool> outBool;
        [SerializeField] private bool boolValue;

        private void Awake()
        {
            outBool.Data = boolValue;
        }
    }
}