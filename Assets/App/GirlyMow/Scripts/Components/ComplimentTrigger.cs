﻿using System;
using App.GirlyMow.States;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class ComplimentTrigger : CoreComponent
    {
        public InputComponent<TMPLink> tmpLink;
        public InputComponent<MoneyProvider> moneyProvider;
        public InputComponent<GameStateProvider> gameStateProvider;
        public int[] moneyTrigger = new[] { 200, 400, 600 };
        private int oldMoney = 0;

        private void Start()
        {
            moneyProvider.Component.MoneyLadder = 0;
        }

        private void Update()
        {
            if (oldMoney < moneyTrigger[0] && moneyProvider.Component.MoneyLadder >= moneyTrigger[0])
            {
                tmpLink.Component.Text.fontSizeMax = 80;
                tmpLink.Component.Text.text = "<bounce a=4><rainb f = 1>WOW</>";
            }

            if (oldMoney < moneyTrigger[1] && moneyProvider.Component.MoneyLadder >= moneyTrigger[1])
            {
                tmpLink.Component.Text.fontSizeMax = 96;
                tmpLink.Component.Text.text = "<bounce a=6><rainb f = 1.5>AWESOME</>";
            }

            if (oldMoney < moneyTrigger[2] && moneyProvider.Component.MoneyLadder >= moneyTrigger[2])
            {
                tmpLink.Component.Text.fontSizeMax = 108;
                tmpLink.Component.Text.text = "<bounce a=10><rainb f = 2>YOU ROCK</>";
            }

            if (gameStateProvider.Component.scriptableGameState.Model.State == GameState.Win)
                tmpLink.Component.Text.text = "";
            
            oldMoney = moneyProvider.Component.MoneyLadder;
        }
    }
}