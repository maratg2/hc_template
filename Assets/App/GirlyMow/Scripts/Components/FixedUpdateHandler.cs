using System;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("HW1")]
    [DisallowMultipleComponent]
    public class FixedUpdateHandler : CoreComponent
    {
        public OutputSignal OnFixedUpdate;

        private void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }
    }
}