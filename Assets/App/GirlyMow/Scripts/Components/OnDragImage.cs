﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class OnDragImage : CoreComponent, IPointerDownHandler
    {
        public OutputSignal OnPointer;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointer?.Invoke();
        }
    }
}