﻿using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;
using UnityEngine.Playables;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class TimelineProvider : CoreComponent
    {
        [SerializeField, NotNull] protected PlayableDirector _playableDirector;

        public PlayableDirector playableDirector
        {
            get => _playableDirector;
            set => _playableDirector = value;
        }
    }
}