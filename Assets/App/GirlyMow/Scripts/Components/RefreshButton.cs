﻿using System;
using System.Diagnostics.CodeAnalysis;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using TMPro;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    public class RefreshButton : CoreComponent
    {
        [SerializeField, NotNull] private TextMeshProUGUI _buttCost;
        [SerializeField, NotNull] private TextMeshProUGUI _buttLevel;
        [SerializeField, NotNull] private TextMeshProUGUI _distanceCost;
        [SerializeField, NotNull] private TextMeshProUGUI _distanceLevel;
        [SerializeField, NotNull] private TextMeshProUGUI _distanceDescription;
        [SerializeField] private UpgradeType _upgradeType;
        
        public InputSignal RefreshButtons;
        public OutputSignal onButtonsRefreshed;
        private UpgradesProvider _upgradesProvider;

        private void Awake()
        {
            _upgradesProvider = GetComponent<UpgradesProvider>();
            switch (_upgradeType)
            {
                case UpgradeType.Butt:
                    RefreshButt();
                    break;
                case UpgradeType.Distance:
                    RefreshDistance();
                    break;
                default:
                    Debug.LogWarning("Default upgrade type");
                    break;
            }
        }

        private void OnEnable()
        {
            switch (_upgradeType)
            {
                case UpgradeType.Butt:
                    RefreshButtons.InvokeAction += RefreshButt;
                    break;
                case UpgradeType.Distance:
                    RefreshButtons.InvokeAction += RefreshDistance;
                    break;
                default:
                    Debug.LogWarning("Default upgrade type");
                    break;
            }
        }

        private void OnDisable()
        {
            switch (_upgradeType)
            {
                case UpgradeType.Butt:
                    RefreshButtons.InvokeAction -= RefreshButt;
                    break;
                case UpgradeType.Distance:
                    RefreshButtons.InvokeAction -= RefreshDistance;
                    break;
                default:
                    Debug.LogWarning("Default upgrade type");
                    break;
            }
        }

        private void RefreshButt()
        {
            if (_upgradesProvider.ButtLevelModel >= _upgradesProvider.ButtPrices.Count)
            {
                Debug.LogError("Index out of butt prices bounds");
                return;
            }
            
            _buttCost.text = _upgradesProvider.ButtPrices[_upgradesProvider.ButtLevelModel].Value.ToString();
            _buttLevel.text = _upgradesProvider.ButtPrices[_upgradesProvider.ButtLevelModel].Name;
        }

        private void RefreshDistance()
        {
            if (_upgradesProvider.DistanceLevelModel >= _upgradesProvider.DistancePrices.Count)
            {
                Debug.LogError("Index out of distance prices bounds");
                return;
            }
            
            _distanceCost.text = _upgradesProvider.DistancePrices[_upgradesProvider.DistanceLevelModel].Value.ToString();
            _distanceLevel.text = _upgradesProvider.DistancePrices[_upgradesProvider.DistanceLevelModel].Name;
            _distanceDescription.text = "DISTANCE X" + (10 + _upgradesProvider.DistanceLevelModel * 3);
            
            onButtonsRefreshed?.Invoke();
        }
    }
}