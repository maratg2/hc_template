﻿using System;
using System.Diagnostics.CodeAnalysis;
using BzKovSoft.ObjectSlicer;
using BzKovSoft.ObjectSlicer.EventHandlers;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("Girly Mow")]
    [DisallowMultipleComponent]
    public class TriggerBonusProvider : CoreComponent
    {
        public OutputSignal OnTrig;
        public bool isGrass = true;
        [SerializeField] private float _colliderRadius = 2.1f;
        [SerializeField] private SphereCollider _collider;
        [SerializeField] private Transform _bladeTransform;
        [SerializeField] private AudioClip _cutSFX;
        private MoneyProvider _moneyProvider;
        private AudioSource _audioSource;
        public static event Action OnFoodTaken;
        
        private void Awake()
        {
            PlayerPrefs.SetFloat("SpeedMulti", 1f);
            _moneyProvider = GetComponent<MoneyProvider>();
            _audioSource = GetComponent<AudioSource>();
        }

        public void TriggerFood()
        {
            OnFoodTaken?.Invoke();
        }

        private void OnTriggerStay(Collider other)
        {
            BzSliceableCollider sliceable = other.gameObject.GetComponent<BzSliceableCollider>();
            if (sliceable)
            {
                var plane = new Plane(Vector3.up, transform.position);
                sliceable.Slice(plane);
                _audioSource.PlayOneShot(_cutSFX);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Bonus otherBonus = other.GetComponent<Bonus>();
            if (otherBonus)
            {
                //grassCollider.Data = other;
                //OnGrassDestroyed?.Invoke();
                switch(otherBonus.bonusType)
                {
                    case BonusType.Size:
                        otherBonus.EmulateDestroy(true);
                        if(isGrass)
                            _bladeTransform.localScale = new Vector3(700f, 400f, 700f);
                        else
                            _bladeTransform.localScale = new Vector3(0.2f * 3, 0.2f * 3, 0.2f * 3);
                        _collider.radius = _colliderRadius;
                        break;
                    case BonusType.Speed:
                        otherBonus.EmulateDestroy(true);
                        PlayerPrefs.SetFloat("SpeedMulti", 2f);
                        break;
                    case BonusType.Money:
                        _moneyProvider.MoneyLvl += 5;
                        _moneyProvider.Money += 5;
                        otherBonus.EmulateDestroy(true);
                        break;
                    case BonusType.Food:
                        TriggerFood();
                        otherBonus.EmulateDestroy(true);
                        break;
                }
                OnTrig?.Invoke();
            }
        }
    }
}