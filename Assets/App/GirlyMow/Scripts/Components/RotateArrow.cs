using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components.Links;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GirlyMow.Actions
{
    [NodeGraphGroupName("Girly Mow")]
    public class RotateArrow : CoreAction
    {
        public InputComponent<TransformLink> arrowTransform;
        [ActionParameter] public float angleZ;
        [ActionParameter] public float speedRot;
        private bool _isRight;
        
        protected override bool Action()
        {
            float rotZ = arrowTransform.Component.Transform.localRotation.z;

            if (rotZ > angleZ / 100f)
                _isRight = false;
            else if (rotZ < -angleZ / 100f)
                _isRight = true;
            
            if (!_isRight)
                arrowTransform.Component.Transform.localRotation *= Quaternion.Euler(0f, 0f, Time.unscaledDeltaTime * speedRot);
            else
                arrowTransform.Component.Transform.localRotation  *= Quaternion.Euler(0f, 0f, -Time.unscaledDeltaTime * speedRot);
            
            return true;
        }
    }
}