﻿using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GirlyMow.Components
{
    [NodeGraphGroupName("HW1")]
    [DisallowMultipleComponent]
    public class Force : CoreComponent
    {
        [SerializeField] private Vector3 _force;

        public Vector3 ForceValue
        {
            get => _force;
            set
            {
                _force = value;
            }
        }
    }
}
