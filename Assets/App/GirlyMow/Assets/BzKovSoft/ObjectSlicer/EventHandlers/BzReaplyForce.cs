﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BzKovSoft.ObjectSlicer.EventHandlers
{
	[DisallowMultipleComponent]
	class BzReaplyForce : MonoBehaviour, IBzObjectSlicedEvent
	{
		public void ObjectSliced(GameObject original, GameObject resultNeg, GameObject resultPos)
        {
			// we need to wait one fram to allow destroyed component to be destroyed.
			StartCoroutine(NextFrame(original, resultNeg, resultPos));
        }

		private IEnumerator NextFrame(GameObject original, GameObject resultNeg, GameObject resultPos)
		{
			yield return null;

			var oRigid = original.GetComponent<Rigidbody>();
			var aRigid = resultNeg.GetComponent<Rigidbody>();
			var bRigid = resultPos.GetComponent<Rigidbody>();

			if (oRigid == null)
				yield break;

			aRigid.angularVelocity = oRigid.angularVelocity;
			bRigid.angularVelocity = oRigid.angularVelocity;
			aRigid.velocity = oRigid.velocity;
			bRigid.velocity = oRigid.velocity;

			bRigid.isKinematic = false;
			
			Vector3 dirVector3 = new Vector3(0.5f, 1f, 0.5f);
        
			if (Random.Range(0f, 1f) <= 0.5f)
				dirVector3.x = 0.5f + Random.Range(0f, 0.3f);
			else
				dirVector3.x = -0.5f - Random.Range(0f, 0.3f);
        
			if (Random.Range(0f, 1f) <= 0.5f)
				dirVector3.z = 0.5f + Random.Range(0f, 0.3f);
			else
				dirVector3.z = -0.5f - Random.Range(0f, 0.3f);
			
			bRigid.AddForce(dirVector3 * 200f);
			bRigid.useGravity = true;
			Material bMat = bRigid.GetComponent<MeshRenderer>().material;
			float t = 0f;
			while (t < 1f)
			{
				bMat.color = new Color(bMat.color.r, bMat.color.g, bMat.color.b, 1f - t);
				t += Time.deltaTime / 1.3f; //1.3 -> time
				yield return new WaitForEndOfFrame();
			}
			bRigid.gameObject.SetActive(false);
		}
	}
}
